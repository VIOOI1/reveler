import { VoidComponent } from "solid-js";

import { Menu } from "./menu";
import { Search } from "./search";

export const Header: VoidComponent = () => {
	return (
		<div 
			class="
			w-screen h-12 bg-dark-700
			flex justify-between items-center
			box-border px-20
			fixed top-0 left-0
			" 
		>
			<div 
				class="
				rounded-full h-8 px-5
				flex-center text-3xl text-white
				font-[Roboto] font-extrabold
				"
			>
				REVELER
			</div>

			<Menu />

			<Search />
		</div>
	);
};
