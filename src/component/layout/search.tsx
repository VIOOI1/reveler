import { Component, onMount } from "solid-js";

import { changeFilter } from "../../stores/homeStore";

export const Search: Component = () => {
	onMount(() => {
		changeFilter("");
	});
	return (
		<input 
			class="
				border-light-300 text-white
				border-2 outline-0
				rounded-full h-7 px-5
				bg-dark-700
				flex-center
				focus:w-3/12 focus:border-none
				placeholder:text-white
				transition-all
				text-lg
				"
			type="text"
			placeholder="Поиск..."
			onInput={(event) => {
				changeFilter(event.target.value);
			}}
		/ >
	);
};
