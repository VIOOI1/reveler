import { useUnit } from "effector-solid";
import { Component, For } from "solid-js";

import { $pages } from "../../stores/homeStore";

export const Menu: Component = () => {
	const pages = useUnit($pages);

	return (
		<ul
			class="
				flex-center gap-x-10
				list-none
				font-[Roboto]
				p-0 m-0
			"
			style={{
				"list-style-type": "none",
			}}
		>
			<For each={pages()}>{ page =>
				<a 
					href={page.link}
					class={`
					no-underline hover:font-extrabold
					text-xl
					${ page.isRead 
			? "text-white" 
			: "text-neutral-300 pointer-events-none" 
		}
					`}
				>
					<li class="text-inherit">{page.title}</li>
				</a>
			}</For>
		</ul>
	);
};
