import { A, useParams } from "@solidjs/router";
import { useUnit } from "effector-solid";
import { For, onMount, VoidComponent } from "solid-js";
import toHtml from "string-to-html";

import { $htmlSlide, getSlide } from "../../stores/presStore";

import { Slide } from "./Slide";
import { Slider } from "./Slider";


export const GenSlide: VoidComponent = () => {
	const htmlSlide = useUnit($htmlSlide);
	const params = useParams();

	onMount(() => { getSlide(params.name); });

	return (
		<>
			<Slider>
				<For each={htmlSlide()}>{ (s, index) => 
					<Slide 
						bgColor={s.bgColor} 
						grid={s.grid || "text-center"} 
						textColor={s.textColor}
						cclass={`index-${index()}`}
					>
						{ toHtml( s.content ) }
					</Slide>
				}</For>
			</Slider>
			<A 
				href="/"
				class="p-2 py-1 font-[Roboto] no-underline font-bold text-white bg-blue-800 fixed bottom-5 left-5 rounded-md"
			>
				Главаня
			</A>
		</>
	);
};
