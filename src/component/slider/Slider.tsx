import { useUnit } from "effector-solid";
import { createEffect, on, onCleanup, onMount, ParentComponent } from "solid-js";

import {
	$curentSlide,
	$slideNumber,
	$transform,
	newSlideNumber,
	newWindowSize,
	nextSlide,
	prevSlide,
} from "../../stores/sliderStore";


// TODO: Сделать сетку для слайдов

export const Slider: ParentComponent 
	= ({ children }: { children: Array<Element> }) => {
		const currentSlide = useUnit($curentSlide);
		const transorm = useUnit($transform);
		const slideNumber = useUnit($slideNumber);

		onMount(() => {
			// console.log(children());
		
			newWindowSize({
				width: window.innerWidth,
				height: window.innerHeight,
			});
			newSlideNumber(children.length);
		});
		createEffect(on(children, (v) => {
			newSlideNumber(v.length);
		}));

		window.addEventListener("resize", () => {
			newWindowSize({
				width: window.innerWidth,
				height: window.innerHeight,
			});
		});

		const keyChange = (event: KeyboardEvent) => {
			if (event.keyCode == 74 || event.keyCode == 40 ) { nextSlide(); }
			if (event.keyCode == 75 || event.keyCode == 38 ) { prevSlide(); }

			if (event.keyCode == 70 ) {
				document.documentElement.requestFullscreen();
			}
		};

		onMount(() => {
			window.addEventListener("keydown", keyChange);
		});
		onCleanup(() => {
			window.removeEventListener("keydown", keyChange);
		});

		return (
			<section class="w-100vw h-100vh overflow-hidden box-border">
				<div 
					class="transition-all"
					style={{
						transform: `translate(-0%, -${transorm()}px) scale(1)`,
					}}
				>
					{ children }
				</div>
				<div
					class="
				w-10 bg-sky-00 text-white
				fixed bottom-5 right-7
				flex justify-center items-center
				cursor-pointer
				"
					onClick={() => nextSlide()}
				>
					<svg xmlns="http://www.w3.org/2000/svg" class="hover:fill-[#3b82f6] fill-[#2563eb]" width="30" height="17.153" viewBox="0 0 30 17.153" > <path id="Icon_ionic-ios-arrow-down" data-name="Icon ionic-ios-arrow-down" d="M21.193,23.229,32.537,11.876a2.135,2.135,0,0,1,3.028,0,2.162,2.162,0,0,1,0,3.037L22.712,27.775a2.14,2.14,0,0,1-2.956.063L6.813,14.922a2.144,2.144,0,0,1,3.028-3.037Z" transform="translate(-6.188 -11.246)"/>
					</svg>
				</div>
				<div
					class="
				w-10 bg-sky-00 text-white
				fixed bottom-15 right-7
				flex justify-center items-center
				cursor-pointer
				"
					onClick={() => prevSlide()}
				>
					<svg xmlns="http://www.w3.org/2000/svg" class="hover:fill-[#3b82f6] fill-[#2563eb]" width="30" height="17.151" viewBox="0 0 30 17.151" > <path id="Icon_ionic-ios-arrow-up" data-name="Icon ionic-ios-arrow-up" d="M21.191,16.421,32.534,27.772a2.144,2.144,0,1,0,3.028-3.037L22.71,11.875a2.14,2.14,0,0,0-2.956-.063L6.813,24.727A2.144,2.144,0,1,0,9.84,27.763Z" transform="translate(-6.188 -11.251)"/>
					</svg>
				</div>
				<div 
					class="fixed top-0 left-0 h-1 bg-blue-500"
					style={{
						width: `${100 / slideNumber() * (currentSlide() + 1)}vw`,
					}}
				>
				</div>
			</section>
		);
	};
