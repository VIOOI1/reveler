import { ParentComponent } from "solid-js";

import "../style/slideGrid.scss";

type GridSlideType = 
	| "cover-sheet"
	| "title-only"
	| "two-there-title"
	| "two-no-title"
	| "cube"
	| "text-center"
	| "two-and-one"
	;

type SlidePropsType = {
	cclass?: string,
	bgColor?: string,
	textColor?: string,
	grid?: GridSlideType,
}

export const Slide: ParentComponent<SlidePropsType> 
	= ({
		children,
		cclass = "",
		grid = "title-only",
		bgColor = "#141414",
		textColor = "#ffffff",
	}) => {
		return (
			<section 
				style={{ 
					background: bgColor,
					color: textColor,
				}}
				class={`
				w-97.7vw h-100vh
				font-[Roboto]
				p-5 border-box
				select-none
				 ${cclass} ${grid}
			`}
			>
				{ children }
			</section>
		);
	};
