/* eslint-disable max-len */
import { createEffect, createEvent, createStore, sample } from "effector";
import { createClient } from "@supabase/supabase-js";

import { predInfo } from "../present/predstavlenie_info";
import { prizObrInfo } from "../present/prinzipi_obr_info";


type GridSlideType = 
	| "cover-sheet"
	| "title-only"
	| "two-there-title"
	| "two-no-title"
	| "cube"
	| "text-center"
	| "two-and-one"
	;
export type PresentationType = {
	grid?: GridSlideType,
	bgColor?: string,
	textColor?: string,
	content: ChildrenType[],
}
export type ChildrenType = {
	tag: 
		| "div"
		| "p"
		| "img"
		| "h1" | "h2" | "h3"
		| "span"
		| "ul" | "li"
	,
	class?: string,
	fn?: () => string,
	content: string | ChildrenType[],
}
type ArrayPresentType = {
	[key: string]: PresentationType[],
}

export const getSlide = createEvent<string>();

const getSlideFx = createEffect<string, PresentationType[]>( async (pres: string) => {
	const supabase = createClient(
		import.meta.env.VITE_SUPABASE_LINK,
		import.meta.env.VITE_SUPABASE_KEY,
	);
	const { data } = await supabase.from("Presentation")
		.select("present")
		.eq("link", pres);
	
	return JSON.parse(data[0].present as string);
} );

export const $htmlSlide = createStore<PresentationType[]>([]);

sample({
	source: getSlide,
	target: getSlideFx,
});

sample({
	source: getSlideFx.doneData,
	fn: (src) => {
		const t: PresentationType[] = src.map(elem => ({
			...elem,
			content: getHTML(elem.content),
		}) as unknown as PresentationType);
		console.log(t);
		return t;
	},
	target: $htmlSlide,
});

const getHTML = (element: ChildrenType[]): string => {
	if ( Array.isArray(element) ) {
		const elem = element.flatMap( i => {
			if ( typeof i.content === "string" ) {
				if (i.tag === "img") {
					return `<img 
							class="${i.class}" 
							src="${i.content}" 
							alt=""
						/>`;
				}
				let str = `
				<${i.tag} class="${i.class}">
					${i.content}
				</${i.tag}>`;

				if (typeof i.fn !== "undefined") {
					str = str.replaceAll("{fn}", i.fn());
				}
				return str;
			} 
			else if ( typeof i.content !== "string" ) {
				// eslint-disable-next-line max-len
				const str = `
				<${i.tag} class="${i.class}">
					${ getHTML(i.content as ChildrenType[]) }
				</${i.tag}>`;
				return str;
			}
			else { return ""; }
		});
		return elem.join(" ");
	} else {
		return "";
	}
};
