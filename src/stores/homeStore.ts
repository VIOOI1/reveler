import { createEffect, createEvent, createStore, restore, sample } from "effector";
import { createClient } from "@supabase/supabase-js";


// ====    ====    ====    ====    Event

export const changeFilter = createEvent<string>();
export const getPresents = createEvent();

// ====    ====    ====    ====    Effects

const getSlideFx = createEffect( async () => {
	const supabase = createClient(
		"https://hdjxlbvslyjkbxvmmejg.supabase.co",
		"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImhkanhsYnZzbHlqa2J4dm1tZWpnIiwicm9sZSI6ImFub24iLCJpYXQiOjE2Njk3MTc0MDksImV4cCI6MTk4NTI5MzQwOX0.I-fs1KOY2TAXFfbUtXQtEvPGAUULoyQgcpcLKHMnSZk",
	);
	const { data: Presentation, error } = await supabase
		.from("Presentation")
		.select("*");
	console.log(Presentation, "---------");
	return Presentation;
	
} );


// ====    ====    ====    ====    Store

type PageStoreType = { title: string, link: string, isRead: boolean };
export const $pages = createStore<PageStoreType[]>([
	{ title: "Главная", link: "/", isRead: true },
	{ title: "Курсы", link: "/", isRead: false },
	{ title: "Темы", link: "/", isRead: false },
	{ title: "Наше будущее", link: "/", isRead: false },
]);

type PresFilterType = {
	title: string,
	description: string,
	author: string,
	link: string,
	created_at: string,
	// creationDate: Date,
}

export const $presents = createStore<PresFilterType[]>([])
	.on(getSlideFx.doneData, (state, data) => {
		return data;
	});
// $presents.watch((data) => console.log(data, "$presents"));

export const $filter = createStore<PresFilterType[]>([]);
// $filter.watch((state) => console.log("-->", state));


sample({
	source: $presents,
	clock: [ changeFilter ],
	fn: (sourseData: PresFilterType[], clockData: string) => {
		return sourseData.filter( pres => {
			return pres.title.toLowerCase().includes(clockData.toLowerCase());
		} );
	},
	target: $filter,
});

sample({
	clock: getPresents,
	target: getSlideFx,
});

sample({
	source: $presents,
	target: $filter,
});

