import { createEvent, createStore, restore, sample } from "effector";

type windowSizeType = { width: number, height: number, }

// ====    ====    ====    ====    Event

export const nextSlide = createEvent();
export const prevSlide = createEvent();

export const newSlideNumber = createEvent<number>();

export const newWindowSize = createEvent<windowSizeType>();
export const clearSlide = createEvent();

// ====    ====    ====    ====    Store

export const $slideNumber = restore( newSlideNumber, 0 );

$slideNumber.reset(clearSlide);

export const $curentSlide = createStore<number>(0)
	.on( nextSlide, (s) => {
		if (s < $slideNumber.getState() - 1) { return s + 1; }
		else { return s; }
	} )
	.on( prevSlide, (s) => {
		if (s !== 0) { return s - 1; }
		else { return s; }
	} )
	.reset( clearSlide );

const $windowSize = restore<windowSizeType>( 
	newWindowSize,
	{ width: 0, height: 0 }, 
);
$windowSize.reset( clearSlide );

export const $transform = createStore(0);

$transform.reset(clearSlide);

sample({
	source: {
		cs: $curentSlide,
		wz: $windowSize,
	},
	fn: (s) => s.wz.height * s.cs + ( 13 * 3 * s.cs ),
	target: $transform,
});

// ====    ====    ====    ====    Effects



