import { A } from "@solidjs/router";
import { useUnit } from "effector-solid";
import { Component, For, onMount } from "solid-js";

import { Header } from "./component/layout/header";
import { $filter, getPresents } from "./stores/homeStore";
import { clearSlide } from "./stores/sliderStore";

export const App: Component = () => {
	const filter = useUnit($filter);

	onMount(() => {
		if (filter().length == 0) {
			getPresents();
		}
		clearSlide();
	});
	
	return (
		<div class=" pt-12"> 
			<Header />
			<div 
				class=" w-screen h-full bg-dark-300 flex items-start "
				style={{ "min-height": "calc(100vh - 48px)" }}
			>
			
				<div class=" w-4/12 p-3 gap-y-3 flex flex-col " >
					<For each={filter()}>{ pres =>
						<div class="
						p-3 rounded-lg shadow-md
						bg-dark-500 font-[Roboto]
						text-white
						">
							<A 
								class="m-0 font-bold no-underline text-white"
								href={`/present/${pres.link}`}>
								<h2 class="m-0 text-2xl mb-2 leading-7">{ pres.title }</h2>
							</A>
							<p class="text-gray-300 m-0">{ pres.description }</p>
							<p class=" m-0">{ /* pres.author */ }</p>
						</div>
					}</For>
				</div>
				<div class="w-2/12 p-3 gap-y-3 flex flex-col min-h-80vh mt-3">
				</div>
				<div class="w-5/12 p-3 gap-y-3 flex flex-col min-h-80vh mt-3 font-[Roboto]">
					<h2 class="text-white text-4xl">
					Как использовать презентации?
					</h2>
					<ol class="text-white text-xl leading-6 w-80%">
						<li>Найдите нужную презентациюв списке или введите её название в поиске</li>
						<li>При нажатии на название вы откроете эту презентацию и можете её просмативать</li>
						<li>Стрелками вверх <span class="keyboard">↑</span> вниз <span class="keyboard">↓</span> вы переключаете слайды</li>
						<li>При нажатии на кнопку <span class="keyboard">F</span> презентация откроется во весь экран</li>
					</ol>
				</div>

			</div>
		</div>
	);
};
