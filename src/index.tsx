import "uno.css";

import { render } from "solid-js/web";
import { Route, Router, Routes } from "@solidjs/router";

import { App } from "./App";
import { GenSlide } from "./component/slider/genSlide";


render(() => (
	<Router>
		<Routes>
			<Route path="/" component={App} />

			<Route path="/present/:name" component={GenSlide} />
			<Route path="/present/test" component={App} />

		</Routes>
	</Router>
), document.getElementById("root") as HTMLElement);
