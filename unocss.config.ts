import { defineConfig } from "@unocss/vite";
import { presetMini } from "@unocss/preset-mini";

export default defineConfig({
	presets: [ presetMini() ],
	shortcuts: {
		"flex-center": "flex justify-center items-center",
		"flex-col-center": "flex flex-col justify-center items-center",
		"keyboard": "bg-dark-400 inline-block px-3 py-0.5 rounded",
	},
	rules: [
		[ /^wh-(\d+)$/, ([ , d ]) => ({ 
			width: `${d / 4}rem`,
			height: `${d / 4}rem`,
		}) ],
	],
});
